package com.example.goenjoy.CU04_ListaFavoritos;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import com.example.goenjoy.model.Museo;
import com.example.goenjoy.room.MuseoDao;
import com.example.goenjoy.room.MuseoDatabase;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.platform.app.InstrumentationRegistry;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class MuseoDAOTest {
    private MuseoDao museoDao;
    private MuseoDatabase mDatabase;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();


    @Before
    public void createMuseoDatabase() {
        //create a mock database, using our RoomDatabase implementation (AppDatabase.class)
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mDatabase = Room.inMemoryDatabaseBuilder(context, MuseoDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        museoDao = mDatabase.museoDao();
    }

    @After
    public void closeDb() throws IOException {
        //closing database
        mDatabase.close();
    }

    @Test
    public void getAllFavTest() throws Exception {
        Museo m1 = CrearMuseo(1);
        Museo m2 = CrearMuseo(2);
        Museo m3 = CrearMuseo(3);

        museoDao.insert(m1);
        museoDao.insert(m2);
        museoDao.insert(m3);

        m1.setFav(1);
        m2.setFav(1);
        m3.setFav(1);

        museoDao.update(m1);
        museoDao.update(m2);
        museoDao.update(m3);


            LiveData<List<Museo>> livemuseos = museoDao.getAllFav();
        List<Museo> museos = LiveDataTestUtils.getValue(livemuseos);
        assertEquals(3,museos.size());
    }


    @Test
    public void deleteAllTest() throws Exception {
        Museo m1 = CrearMuseo(1);
        Museo m2 = CrearMuseo(2);
        Museo m3 = CrearMuseo(3);

        museoDao.insert(m1);
        museoDao.insert(m2);
        museoDao.insert(m3);

        m1.setFav(1);
        m2.setFav(1);
        m3.setFav(1);

        museoDao.update(m1);
        museoDao.update(m2);
        museoDao.update(m3);

        museoDao.deleteAll();

        LiveData<List<Museo>> livemuseos = museoDao.getAllFav();
        List<Museo> museos = LiveDataTestUtils.getValue(livemuseos);
        assertEquals(3,museos.size());
    }

    public static Museo CrearMuseo(int id){
        Museo museo = new Museo(id, "title", "relation", "localidad", "postalCode", "streetAdress",
                40.414358466555235f, -3.6974741545860015f, "desc", "0", "schedule", 0, 0, 0,0);
        return museo;
    }

}
