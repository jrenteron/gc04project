package com.example.goenjoy.CU12_VerPerfil;


import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.goenjoy.model.Perfil;
import com.example.goenjoy.room.MuseoDatabase;
import com.example.goenjoy.room.PerfilDao;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;


@RunWith(AndroidJUnit4.class)
public class PerfilDAOUnitTest {

    private PerfilDao perfilDAO;
    private MuseoDatabase mDatabase;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();


    @Before
    public void createMuseoDatabase() {
        //create a mock database, using our RoomDatabase implementation (AppDatabase.class)
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mDatabase = Room.inMemoryDatabaseBuilder(context, MuseoDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        perfilDAO = mDatabase.perfilDao();
    }

    @After
    public void closeDb() throws IOException {
        //closing database
        mDatabase.close();
    }

    @Test
    public void writePerfilAndReadInDataBase() throws Exception {
        Perfil perfil = CrearPerfil(1);
        perfilDAO.insert(perfil);


        LiveData<List<Perfil>> liveperfiles = perfilDAO.get();
        List<Perfil> perfiles = LiveDataTestUtils.getValue(liveperfiles);

        assertEquals(1, perfiles.size());
    }


    public static Perfil CrearPerfil(int id){
        Perfil p = new Perfil();
        p.setId(id);
        p.setNombre("Federico");
        p.setEmail("federicopizarro@gmail.com");
        return p;
    }
}