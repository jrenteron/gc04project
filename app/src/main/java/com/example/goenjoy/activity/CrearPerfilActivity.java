package com.example.goenjoy.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.goenjoy.R;
import com.example.goenjoy.model.Perfil;
import com.example.goenjoy.viewmodel.PerfilViewModel;

public class CrearPerfilActivity extends AppCompatActivity {
    EditText et_nombreUsuario, et_correo;
    PerfilViewModel mPerfilViewModel;
    ImageView bAtras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_perfil);

        et_nombreUsuario = findViewById(R.id.nombreUsuario);
        et_correo = findViewById(R.id.email);
        Button bCrear = findViewById(R.id.bCrear);
        bCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrearPerfilActivity.this, LugaresActivity.class);
                String nombreUsuario = et_nombreUsuario.getText().toString().trim();
                String correo = et_correo.getText().toString().trim();
                mPerfilViewModel = ViewModelProviders.of(CrearPerfilActivity.this).get(PerfilViewModel.class);
                mPerfilViewModel.insert(new Perfil(1, nombreUsuario, correo));
                startActivity(intent);
            }
        });

        bAtras = (ImageView) findViewById(R.id.bAtras);
        bAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}