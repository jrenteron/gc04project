package com.example.goenjoy.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.goenjoy.R;
import com.example.goenjoy.fragment.MuseosFragment;
import com.example.goenjoy.model.Museo;
import com.example.goenjoy.model.Perfil;
import com.example.goenjoy.viewmodel.MuseosViewModel;
import com.example.goenjoy.viewmodel.PerfilViewModel;

import java.util.List;

public class EditarPerfilActivity extends AppCompatActivity {

    PerfilViewModel mPerfilViewModel;
    Button bModificar, bBorrar;
    EditText etNombreUsuario, etCorreo;
    ImageView bAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bModificar = findViewById(R.id.bModificar);
        bBorrar = findViewById(R.id.bBorrar);
        etNombreUsuario = findViewById(R.id.etNombreUsuario);
        etCorreo = findViewById(R.id.etCorreo);


        bModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditarPerfilActivity.this, LugaresActivity.class);
                String nombreUsuario = etNombreUsuario.getText().toString().trim();
                String correo = etCorreo.getText().toString().trim();
                Perfil p = new Perfil(1,nombreUsuario, correo);
                mPerfilViewModel = ViewModelProviders.of(EditarPerfilActivity.this).get(PerfilViewModel.class);
                mPerfilViewModel.insert(p);
                startActivity(intent);
            }
        });

        bBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditarPerfilActivity.this, LugaresActivity.class);
                mPerfilViewModel.delete();
                startActivity(intent);
            }
        });
        mPerfilViewModel = ViewModelProviders.of(this).get(PerfilViewModel.class);
        mPerfilViewModel.get().observe(this, new Observer<List<Perfil>>() {
            @Override
            public void onChanged(List<Perfil> p) {
                if(p.isEmpty()){ // si no existe ningún perfil creado, nos lleva a crear uno.
                    Intent intent = new Intent(EditarPerfilActivity.this, CrearPerfilActivity.class);
                    startActivity(intent);
                    finish();
                }else { // aquí se edita el perfil.
                    etNombreUsuario.setText(p.get(0).getNombre());
                    etCorreo.setText(p.get(0).getEmail());
                }
            }
        });
        bAtras = (ImageView) findViewById(R.id.bAtras);
        bAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
