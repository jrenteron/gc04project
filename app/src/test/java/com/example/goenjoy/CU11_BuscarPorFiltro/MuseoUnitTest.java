package com.example.goenjoy.CU11_BuscarPorFiltro;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class MuseoUnitTest {

    @Test
    public void setTipoTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setTipo(value);
        final Field field = instance.getClass().getDeclaredField("tipo");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getTipoTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("tipo");
        field.setAccessible(true);
        field.set(instance, 1);

        //when
        final int result = instance.getTipo();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }
}
