package com.example.goenjoy.CU16_VerHorario;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class MuseoUnitTest {

    @Test
    public void setScheduleTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "De 8:00 a 14:00";
        com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        instance.setSchedule(value);
        final Field field = instance.getClass().getDeclaredField("schedule");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getScheduleTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Museo instance = new  com.example.goenjoy.model.Museo();
        final Field field = instance.getClass().getDeclaredField("schedule");
        field.setAccessible(true);
        field.set(instance, "De 8:00 a 14:00");

        //when
        final String result = instance.getSchedule();

        //then
        assertEquals("field wasn't retrieved properly", result, "De 8:00 a 14:00");
    }
}
