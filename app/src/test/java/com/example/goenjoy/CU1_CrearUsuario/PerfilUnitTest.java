package com.example.goenjoy.CU1_CrearUsuario;

import org.junit.Test;

import java.lang.reflect.Field;
import com.example.goenjoy.model.Perfil;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PerfilUnitTest {

    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 506;
        com.example.goenjoy.model.Perfil instance = new  com.example.goenjoy.model.Perfil();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Perfil instance = new  com.example.goenjoy.model.Perfil();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 12);

        //when
        final int result = instance.getId();

        //then
        assertEquals("field wasn't retrieved properly", result, 12);
    }

    @Test
    public void setNombreTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Federico Pizarro";
        com.example.goenjoy.model.Perfil instance = new  com.example.goenjoy.model.Perfil();
        instance.setNombre(value);
        final Field field = instance.getClass().getDeclaredField("nombre");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getNombreTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Perfil instance = new  com.example.goenjoy.model.Perfil();
        final Field field = instance.getClass().getDeclaredField("nombre");
        field.setAccessible(true);
        field.set(instance, "Federico Pizarro");

        //when
        final String result = instance.getNombre();

        //then
        assertEquals("field wasn't retrieved properly", result, "Federico Pizarro");
    }

    @Test
    public void setEmailTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "federicopizarrin@gmail.com";
        com.example.goenjoy.model.Perfil instance = new  com.example.goenjoy.model.Perfil();
        instance.setEmail(value);
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getEmailTest() throws NoSuchFieldException, IllegalAccessException {
        final  com.example.goenjoy.model.Perfil instance = new  com.example.goenjoy.model.Perfil();
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        field.set(instance, "federicopizarrin@gmail.com");

        //when
        final String result = instance.getEmail();

        //then
        assertEquals("field wasn't retrieved properly", result, "federicopizarrin@gmail.com");
    }



}
